package com.example.accessingdatajpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AccessingDataJpaApplication {

	private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			// save a few customers
			repository.save(new Customer("Hello", "World", "abc@gmail.com"));
			repository.save(new Customer("Sanjana", "Moudgalya", "sm@gmail.com"));
			repository.save(new Customer("ABC", "XYZ", "xyz@gmail.com"));
			repository.save(new Customer("Sanjay", "Moudgalya", "sam@gmail.com"));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = repository.findById(1L);
			log.info("Customer found with findById(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Moudgalya'):");
			log.info("--------------------------------------------");
			repository.findByLastName("Moudgalya").forEach(name -> {
				log.info(name.toString());
			});

			//fetch customers by email id
			log.info("Customer found with findByEmail('xyz@gmail.com'):");
			log.info("--------------------------------------------");
			repository.findByEmail("xyz@gmail.com").forEach(email -> {
				log.info(email.toString());
			});

			log.info("");
		};
	}

}
